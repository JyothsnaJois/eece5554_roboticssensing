clc;close all; clear all;
data=readtable('occluded_moving.csv');
east_full=data{:,8}*10^-5;
north_full=data{:,9}*10^-5;
m=mean(east_full);
time=data{:,1};
figure(1);scatter(east_full(1:142),north_full(1:142));xlabel('easting');ylabel('northing');hold on 
figure(2);hist(east_full);title('histogram of easting');
figure(3);hist(north_full);title('histogram of northing');
fix=data{:,12};
data_field=readtable('field_moving.csv');
east_full1=data_field{:,8}*10^-5;
north_full1=data_field{:,9}*10^-5;
timee=data_field{:,1}-min(data_field{:,1});
fixx=data_field{:,12};
figure(20);plot(timee,fixx);xlabel('time');ylabel('fix');
figure(7);scatter(east_full1,north_full1);xlabel('easting');ylabel('northing');
title('histogram of easting');figure(8);hist(east_full1);
title('histogram of northing');figure(9);hist(north_full1);
figure(31);plot(time,fix);xlabel('time');ylabel('fix');
figure(10);plot(east_full1-east_full1(1),north_full1-north_full1(1),'*');xlabel('easting');ylabel('northing');
hold on
lm2=fitlm(east_full1(1:194),north_full1(1:194));
y=lm2.Coefficients.Estimate(1);
slope=lm2.Coefficients.Estimate(2);
x2=linspace(min(east_full1(1:194)),max(east_full1(1:194)),1000);
y2=y+slope*x2;
plot(x2-east_full1(1),y2-north_full1(1),'r-','LineWidth',2);
lm3=fitlm(east_full1(195:281),north_full1(195:281));
y1=lm3.Coefficients.Estimate(1);
slope2=lm3.Coefficients.Estimate(2);
x3=linspace(min(east_full1(1:195)),max(east_full1(1:195)),1000);
y3=y1+slope2*x3;
plot(x3-east_full1(1),y3-north_full1(1),'r-','LineWidth',2);hold off
