clc;
close all;
clear all;
datai=readtable('imuindoor.csv');
datag=readtable('magindoor.csv');
dataimu=table2array(datai);
datamag=table2array(datag);
quat_x=dataimu(:,5);
quat_y=dataimu(:,6);
quat_z=dataimu(:,7);
quat_w=dataimu(:,8);
angvel_x=dataimu(:,18);
angvel_y=dataimu(:,19);
angvel_z=dataimu(:,20);
linearacc_x=dataimu(:,30);
linearacc_y=dataimu(:,31);
linearacc_z=dataimu(:,32);
mag_x=datamag(:,5);
mag_y=datamag(:,6);
mag_z=datamag(:,7);
% % histograms
magx=hist(mag_x);
figure(1);title('histogram of magnetometer x axis');plot(magx);title('histogram of magnetometer x axis');
magy=hist(mag_y);
figure(2);title('histogram of magnetometer y axis');plot(magy);title('histogram of magnetometer y axis');
magz=hist(mag_z);
figure(3);title('histogram of magnetometer x axis');plot(magz);title('histogram of magnetometer z axis');
angx=hist(angvel_x);
figure(4);title('histogram of magnetometer x axis');plot(angx);title('histogram of angular velocity x axis');
angy=hist(angvel_y);
figure(5);title('histogram of magnetometer x axis');plot(angy);title('histogram of angular vel y axis');
angz=hist(angvel_z);
figure(6);title('histogram of magnetometer x axis');plot(angz);title('histogram of angular vel z axis');
linx=hist(linearacc_x);
figure(7);title('histogram of magnetometer x axis');plot(linx);title('histogram of linear acceleration x axis');
liny=hist(linearacc_y);
figure(8);title('histogram of magnetometer x axis');plot(liny);title('histogram of linear acceleration y x axis');
linz=hist(linearacc_z);
figure(8);title('histogram of magnetometer x axis');plot(linz);title('histogram of linear acceleration z axis');



