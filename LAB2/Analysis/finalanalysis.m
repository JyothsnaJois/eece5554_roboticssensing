%% Calibrating the magnetometer
clc;
clear all;close all;
datai=readtable('imudrive.csv');
datag=readtable('magdrive.csv');
dataimu=table2array(datai);
datamag=table2array(datag);
quat_x=dataimu(:,5);
quat_y=dataimu(:,6);
quat_z=dataimu(:,7);
quat_w=dataimu(:,8);
angvel_x=dataimu(:,18);
angvel_y=dataimu(:,19);
angvel_z=dataimu(:,20);
linearacc_x=dataimu(:,30);
linearacc_y=dataimu(:,31);
linearacc_z=dataimu(:,32);
magx=datamag(:,5);
magy=datamag(:,6);
magz=datamag(:,7);
mag_xcir=magx(1500:7000);
mag_ycir=magy(1500:7000);
mag_zcir=magz(1500:7000);
mag_xst=magx(7000:40000);
mag_yst=magy(7000:40000);
mag_zst=magz(7000:40000);
axis equal
% figure(1);
% scatter3(mag_xcir,mag_ycir,mag_zcir,'r');
% axis equal  
D=[mag_xcir(:),mag_ycir(:),mag_zcir(:)];
D1=[magx(:),magy(:),magz(:)];
[A,b,expMFS]=magcal(D);
xCorrected = (D-b)*A;
figure(2);
de = HelperDrawEllipsoid;
de.plotCalibrated(A,b,expMFS,D,xCorrected,'Diag');

% figure(3);
% plot(xCorrected(:,1),xCorrected(:,2),'b',mag_xcir,mag_ycir,'r');
% axis equal
% xlabel('uT');ylabel('uT');legend('calibrated','original');
%% 
clc;
clear all;close all;
datai=readtable('imudrive.csv');
datag=readtable('magdrive.csv');
dataimu=table2array(datai);
datamag=table2array(datag);
q_x=dataimu(:,5);
q_y=dataimu(:,6);
q_z=dataimu(:,7);
q_w=dataimu(:,8);
q=[q_w,q_x,q_y,q_z];
eulZYX = quat2eul(q)
time=dataimu(:,1);
start=time(1:1);
final_time=(time(:)-start(:))*10^-9;
magx=datamag(:,5);
magx_cir=magx(:);
magy=datamag(:,6);
magz=datamag(:,7);
magy_cir=magy(:);
magz_cir=magz(:);
acc_x=dataimu(:,30);
acc_y=dataimu(:,31);
accx=acc_x(1:500);
accy=acc_y(1:500);
acc_y=dataimu(:,31);
acc_z=dataimu(:,32);
angvel_z=dataimu(:,20);
angvel_x=dataimu(:,18);
angvel_y=dataimu(:,19);
D=[magx_cir(7500:42000),magy_cir(7500:42000),magz_cir(7500:42000)];
% D1=[magx(:),magy(:),magz(:)];
[A,b,expMFS]=magcal(D);
xCorrected = (D-b)*A;
mx=xCorrected(:,1);
my=xCorrected(:,2);
mz=xCorrected(:,3);
meanaccx=mean(accx(:));
meanaccy=mean(accy(:));
finalaccx=acc_x(:)-meanaccx(:);
finalaccy=acc_y(:)-meanaccy(:);
velx=cumtrapz(final_time(:),finalaccx(:));
velxx=cumtrapz(final_time(:),acc_x(:));
figure(4);plot(final_time(:),velxx(:));title('forward velocity IMU before correction');xlabel('time');ylabel('velocity');
figure(5);plot(final_time(:),velx(:));title('forward velocity IMU');xlabel('time');ylabel('velocity');
vely=cumtrapz(final_time(:),finalaccx(:));
accel=[acc_x(7500:42000),acc_y(7500:42000),acc_z(7500:42000)];
gyro=[angvel_x(7500:42000),angvel_y(7500:42000),angvel_z(7500:42000)];
magneto=[mx(:),my(:),mz(:)];
yaw_old=atan(my(:)./mx(:));
yaw_fil=lowpass(yaw_old,0.01);
yaw_new=unwrap(yaw_old);
figure(6);
plot(final_time(7500:42000),yaw_old(:));ylabel('yaw(rad)');xlabel('time(s)');
figure(7);
plot(final_time(7500:42000),yaw_new(:));xlabel('time(s)');ylabel('yaw(rad)');

FUSE=complementaryFilter;
FUSE=complementaryFilter;
[orientation,angularVelocity]=FUSE(accel,gyro,magneto);
q=eulerd(orientation,'XYZ','frame');
figure(3);title('orientation');plot(eulZYX(:,1));title('yaw from imu');xlabel('time');ylabel('yaw');
angvelz=cumtrapz(final_time(7500:42000),angvel_z(7500:42000));
angvelz1=cumtrapz(final_time(7500:42000),angularVelocity(7500:42000));
angvelzz=highpass(angvelz,0.9);
angvelzz=highpass(angvelz,0.9);
yawgyro=unwrap(angvelzz(:));
figure(8);plot(angvelz);title('yaw integrated from the gyrometer');xlabel('time');ylabel('yaw');
yaw=yaw_new(:)+angvelzz(:);
figure(9);plot(final_time(7500:42000),yaw,'r');legend('yaw integrated from gyro','yaw from magnetometer');
q_xx=q_x(7500:42000);
%% Gps 
clc;
clear all;close all;
datag=readtable('gps_driving.csv');
e=datag{:,8}
n=datag{:,9}
time=datag{:,1};
% time=time*10^-18;
% time=dataimu(:,1);
start=time(1:1);
final_time=(time(:)-start(:))*10^-9;
N=length(e);
% histfit(e)
em=mean(e)
ea=e-em
nm=mean(n)
na=n-nm
se=std(e)
sn=std(n)
figure(10);scatter(ea,na);
xlabel('easting in meters');
ylabel('northing in meters');
dis=zeros(length(e),1);
for i=2:N
    dis(i)=sqrt(((e(i)-e(i-1))^2)+((n(i)-n(i-1))^2));
    timee(i)=time(i)-time(i-1);
    velocity(i)=dis(i)./timee(i-1);
end
velocity=velocity*10^-9;
figure(11);title('forward velocity of gps');plot(final_time,velocity);xlabel('time');ylabel('velocity');
%% imu path and acceleration
datai=readtable('imudrive.csv');
datag=readtable('magdrive.csv');
dataimu=table2array(datai);
datamag=table2array(datag);
time=dataimu(:,1);
start=time(1:1);
final_time=(time(:)-start(:))*10^-9;
acc_x=dataimu(:,30);
acc_y=dataimu(:,31);
acc_z=dataimu(:,32);
accx=acc_x(1:500);
accy=acc_y(1:500);
accz=acc_z(1:500);
magx=datamag(:,5);
magy=datamag(:,6);
magz=datamag(:,7);
D=[magx(:),magy(:),magz(:)];
% D1=[magx(:),magy(:),magz(:)];
[A,b,expMFS]=magcal(D);
xCorrected = (D-b)*A;
mx=xCorrected(:,1);
my=xCorrected(:,2);
mz=xCorrected(:,3);
meanaccx=mean(accx(:));
meanaccy=mean(accy(:));
meanaccz=mean(accz(:));
angvel_x=dataimu(:,18);
angvel_y=dataimu(:,19);
angvel_z=dataimu(:,20);
accel=[acc_x(:),acc_y(:),acc_z(:)];
gyro=[angvel_x(:),angvel_y(:),angvel_z(:)];
magneto=[magx(:),magy(:),magz(:)];
finalaccx=acc_x(7000:45000)-meanaccx(:);
finalaccy=acc_y(7000:45000)-meanaccy(:);
finalaccz=acc_z()-meanaccz(:);
velx=cumtrapz(final_time(7000:45000),acc_x(7000:45000));
vely=cumtrapz(final_time(7000:45000),acc_y(7000:45000));
velz=cumtrapz(final_time(7000:45000),acc_z(7000:45000));
pos_x=zeros(1,33001);
pos_y=zeros(1,33001);
yaw_old=atan(my(:)./mx(:));
yaw_new=unwrap(yaw_old(:));
yaw_fil=lowpass(yaw_new,0.01);
q_y=dataimu(:,6);
q_x=dataimu(:,5);
q_z=dataimu(:,7);
q_w=dataimu(:,8);
qua=[q_w,q_x,q_y,q_z];
eul=quat2eul(qua);
yaw=eul(:,1);
% velx=o
for h=2:length(yaw_old(7000:45000))
    pos_x(h)=velx(h).*cosd(yaw_old(h))+pos_x(h-1);
    pos_y(h)=velx(h).*sind(yaw_old(h))+pos_y(h-1);
end
figure(12);plot(pos_x(:),pos_y(:));
dis=cumtrapz(velx(:));
% figure(13);title('displacement');plot(dis(:));
calvel=diff(dis(:));
figure(14);plot(calvel(:));title('acceleration as observed');xlabel('time');ylabel('acc');
calculated=angvel_z(1:38000).*calvel(:);
figure(15);plot(calculated(:));title('acceleration as sensed');
figure(16);title('vel');plot(finalaccy(:));
%% 

